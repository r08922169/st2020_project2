const puppeteer = require('puppeteer');
const url = 'http://localhost:3000'
const openBrowser = true;
const browserOption = {
    headless: !openBrowser,
    defaultViewport: {
        width: 1600,
        height: 900
    },
    args: []
}
const waitAfterLogin = 3000;
const waitAfterSendMessage = 500;
const waitForIframLoading = 4000;
const longTestTimeout = 30000;
const shortTestTimeout = 15000;

const screenFilename = name => {
    return `test/screenshots/${name}.png`;
};

const defaultLogin = async (page, user) => {
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', user);
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1');
    await page.keyboard.press('Enter');
    await page.waitFor(waitAfterLogin);
}

const sendMessage = async (page, message) => {
    await page.type('#message-form > input[type=text]', message);
    await page.click('#message-form > button');
    await page.waitFor(waitAfterSendMessage);
}

// 1 - example
test('[Content] Login page title should exist', async () => {
    const browser = await puppeteer.launch(browserOption);
    const page = await browser.newPage();
    await page.goto(url);

    // await page.screenshot({path: screenFilename("case1")});

    // test
    let title = await page.$eval('body > div > form > div:nth-child(1) > h3', (content) => content.innerHTML);
    expect(title).toBe('Login');

    await browser.close();
}, shortTestTimeout);


// 2
test('[Content] Login page join button should exist', async () => {
    const browser = await puppeteer.launch(browserOption);
    const page = await browser.newPage();
    await page.goto(url);

    // test
    let button = await page.$eval('body > div > form > div:nth-child(4) > button', (content) => content.innerHTML);
    expect(button).toBe('Join');

    // await page.screenshot({path: screenFilename("case2")});

    await browser.close();
}, shortTestTimeout);

// 3 - example
test('[Screenshot] Login page', async () => {
    const browser = await puppeteer.launch(browserOption);
    const page = await browser.newPage();
    await page.goto(url);

    await page.screenshot({path: screenFilename("case3-login")});

    await browser.close();
}, shortTestTimeout);

// 4
test('[Screenshot] Welcome message', async () => {
    const browser = await puppeteer.launch(browserOption);
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John');
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1');
    await page.keyboard.press('Enter');

    // wait for the check picture to show
    await page.waitForSelector(".swal2-success-ring");
    await page.waitFor(500);

    await page.screenshot({path: screenFilename("case4-welcome")});

    await browser.close();
}, shortTestTimeout);

// 5
test('[Screenshot] Error message when you login without user and room name', async () => {
    const browser = await puppeteer.launch(browserOption);
    const page = await browser.newPage();
    await page.goto(url);

    await page.keyboard.press('Enter');
    await page.waitForSelector("#swal2-content");
    await page.waitFor(500);

    await page.screenshot({path: screenFilename("case5-without_user_and_room_name")});

    await browser.close();
}, shortTestTimeout);

// 6
test('[Content] Welcome message', async () => {
    const browser = await puppeteer.launch(browserOption);
    const page = await browser.newPage();
    await page.goto(url);

    await defaultLogin(page, "John");

    // await page.screenshot({path: screenFilename("case6")});

    // test
    let message = await page.$eval('.message__body > p', (content) => content.innerHTML);
    expect(message).toBe('Hi John, Welcome to the chat app');

    await browser.close();
}, shortTestTimeout);

// 7 - example
test('[Behavior] Type user name', async () => {
    const browser = await puppeteer.launch(browserOption);
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', 100);

    // await page.screenshot({path: screenFilename("case7")});

    // test
    let userName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(2) > input[type=text]').value;
    });
    expect(userName).toBe('John');

    await browser.close();
}, shortTestTimeout);

// 8
test('[Behavior] Type room name', async () => {
    const browser = await puppeteer.launch(browserOption);
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', 100);

    // await page.screenshot({path: screenFilename("case8")});

    // test
    let roomName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(3) > input[type=text]').value;
    });
    expect(roomName).toBe('R1');

    await browser.close();
}, shortTestTimeout);

// 9 - example
test('[Behavior] Login', async () => {
    const browser = await puppeteer.launch(browserOption);
    const page = await browser.newPage();

    await page.goto(url);

    await defaultLogin(page, "John");

    // await page.screenshot({path: screenFilename("case9")});

    // test
    let member = await page.evaluate(() => {
        return document.querySelector('#users > ol > li').innerHTML;
    });
    expect(member).toBe('John');

    await browser.close();
}, shortTestTimeout);

// 10
test('[Behavior] Login 2 users', async () => {
    const browser1 = await puppeteer.launch(browserOption);
    const browser2 = await puppeteer.launch(browserOption);
    const page1 = await browser1.newPage();
    const page2 = await browser2.newPage();
    await page1.goto(url);
    await page2.goto(url);

    await defaultLogin(page1, "John");
    await defaultLogin(page2, "Mike");

    // await page1.screenshot({path: screenFilename("case10-user1")});
    // await page2.screenshot({path: screenFilename("case10-user2")});

    // test
    let member1 = await page2.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(1)').innerHTML;
    });
    let member2 = await page2.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(2)').innerHTML;
    });
    expect(member1).toBe('John');
    expect(member2).toBe('Mike');

    await browser1.close();
    await browser2.close();
}, longTestTimeout);

// 11
test('[Content] The "Send" button should exist', async () => {
    const browser = await puppeteer.launch(browserOption);
    const page = await browser.newPage();

    await page.goto(url);

    await defaultLogin(page, 'John');

    // await page.screenshot({path: screenFilename("case11")});

    // test
    let sendButton = await page.evaluate(() => {
        return document.querySelector('#message-form > button').innerHTML;
    });
    expect(sendButton).toBe('Send');

    await browser.close();
}, shortTestTimeout);

// 12
test('[Behavior] Send a message', async () => {
    const browser = await puppeteer.launch(browserOption);
    const page = await browser.newPage();
    await page.goto(url);

    await defaultLogin(page, "John");
    await sendMessage(page, 'sample message');

    // await page.screenshot({path: screenFilename("case12")});

    // test
    let sender = await page.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(2) > div:nth-child(1) > h4').innerHTML;
    });
    let message = await page.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(2) > div:nth-child(2) > p').innerHTML;
    });
    expect(sender).toBe("John");
    expect(message).toBe("sample message");

    await browser.close();
}, shortTestTimeout);

// 13
test('[Behavior] John says "Hi" and Mike says "Hello"', async () => {
    const browser1 = await puppeteer.launch(browserOption);
    const browser2 = await puppeteer.launch(browserOption);
    const page1 = await browser1.newPage();
    const page2 = await browser2.newPage();
    await page1.goto(url);
    await page2.goto(url);

    await defaultLogin(page1, "John");
    await defaultLogin(page2, "Mike");
    await sendMessage(page1, "Hi");
    await sendMessage(page2, "Hello");

    // test John's page
    let message3Sender = await page1.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(3) > div:nth-child(1) > h4').innerHTML;
    });
    let message3Text = await page1.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(3) > div:nth-child(2) > p').innerHTML;
    });
    let message4Sender = await page1.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(4) > div:nth-child(1) > h4').innerHTML;
    });
    let message4Text = await page1.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(4) > div:nth-child(2) > p').innerHTML;
    });
    expect(message3Sender).toBe('John');
    expect(message3Text).toBe('Hi');
    expect(message4Sender).toBe('Mike');
    expect(message4Text).toBe('Hello');

    // test Mike's page
    message3Sender = await page2.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(2) > div:nth-child(1) > h4').innerHTML;
    });
    message3Text = await page2.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(2) > div:nth-child(2) > p').innerHTML;
    });
    message4Sender = await page2.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(3) > div:nth-child(1) > h4').innerHTML;
    });
    message4Text = await page2.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(3) > div:nth-child(2) > p').innerHTML;
    });
    expect(message3Sender).toBe('John');
    expect(message3Text).toBe('Hi');
    expect(message4Sender).toBe('Mike');
    expect(message4Text).toBe('Hello');

    // await page1.screenshot({path: screenFilename("case13-John")});
    // await page2.screenshot({path: screenFilename("case13-Mike")});

    await browser1.close();
    await browser2.close();
}, longTestTimeout);

// 14
test('[Content] The "Send location" button should exist', async () => {
    const browser = await puppeteer.launch(browserOption);
    const page = await browser.newPage();

    await defaultLogin(page, "John");

    // await page.screenshot({path: screenFilename("case14")});

    // test
    let button = await page.evaluate(() => {
        return document.querySelector('#send-location').innerHTML;
    });
    expect(button).toBe('Send location');

    await browser.close();
}, shortTestTimeout);

// 15
test('[Behavior] Send a location message', async () => {
    const browser = await puppeteer.launch(browserOption);
    const page = await browser.newPage();

    // mock API of getting current position
    await page.evaluateOnNewDocument(function() {
        navigator.geolocation.getCurrentPosition = function (callback) {
            setTimeout(() => {
                callback({
                    'coords': {
                        accuracy: 1649,
                        altitude: null,
                        altitudeAccuracy: null,
                        heading: null,
                        latitude: 25.021644799999997,
                        longitude: 121.5463424,
                        speed: null
                    }
                })
            }, 100)
        }
    });

    await defaultLogin(page, "John");

    await page.click('#send-location');
    await page.waitFor(waitForIframLoading);

    // await page.screenshot({path: screenFilename("case15")});

    // test
    // expect to sent a message of maps.google.com API which shows the map
    let messageBody = await page.evaluate(() => {
        return document.querySelector('#messages > li:nth-child(2) > div:nth-child(2) > div > iframe').src;
    });
    api = new URL(messageBody);
    expect(api.hostname).toBe('maps.google.com');

    await browser.close();
}, longTestTimeout);
